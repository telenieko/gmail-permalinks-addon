# Gmail Permalinks Privacy Policy

## Introduction

This privacy policy has been compiled to clarify how data is used by Gmail Permalinks (the addon) what if any information is collected, etc. Please read the remainder of this document carefully to get a clear understanding of data usage in the addon.

### What personal information is collected from users of the addon?

No information is collected by the addon. None. 

### Third-party disclosure

No information is sold, traded or transfered to outside parties.
 
### Cookies

We do not use any cookies at all for ourselves. The addon runs inside Google products which may in turn have their own cookies documented elsewhere.

Neither do we use first-party cookies (such as the Google Analytics cookies) or third-party cookies (such as the DoubleClick cookie) or other third-party identifiers.

### Opting out

N/A, there is nothing to opt out of.

## Contacting Us

Please refer to our [project home](https://gitlab.com/telenieko/gmail-permalinks-addon/) for contact details or to file issues against the project source tree.
