# Gmail permalinks addon

This is a very simple Google Workspace Addon for Gmail that provides you with quick access to a permalink to a gmail thread.

- Source code will be published here soon.
- If you encounter any problems please file an issue.

The addon *does not store any data at all*, so there is no privacy policy published.

# Support

This is an Open Source project, if you run into trouble you may [file an issue](https://gitlab.com/telenieko/gmail-permalinks-addon/-/issues/new) or even submit a push request fixing the problem.

Being Open Source you may clone the repository and create a new deployment of your own code in Google.

# Terms of Service

There are no guarantees in terms of SLA or whatsoever. The published addon runs on my own personal GCP account and may be discontinued at any time. Regardless, you may deploy the code on your own account.
